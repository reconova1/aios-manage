FROM openjdk:8-jdk-alpine
MAINTAINER lijian@reconova.com
WORKDIR /work
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
ADD target/aios-manage-0.0.1-SNAPSHOT-*.jar  app.jar
EXPOSE 18085
